var TheSlider = function(bundleOptions) {
    // bundle
    if (!bundleOptions) return handleError(1);
    if (!bundleOptions.bundle) return;

    var bundleE = bundleOptions.bundle;
    var sliders = {};
    var slidersE = bundleE.querySelectorAll(bundleOptions.sliderSelector);

    var autoplayTimer;

    // handle dots
    var dotsE = bundleOptions.bundle.querySelectorAll('[' + bundleOptions.dotSelector + ']');
    ;[].forEach.call(dotsE, function(dotE) {
        dotE.addEventListener('click', function() {
            clearInterval(autoplayTimer);
            var id = dotE.getAttribute(bundleOptions.dotSelector);
            Object.keys(sliders).forEach(function(sliderId) {
                var slider = sliders[sliderId];
                slider.slideTo(id);
            });
        });
    });

    // handle controls
    if (bundleOptions.controls) {
        if (bundleOptions.controls.prev) {
            bundleOptions.controls.prev.addEventListener('click', function() {
                clearInterval(autoplayTimer);
                Object.keys(sliders).forEach(function(sliderId) {
                    var slider = sliders[sliderId];
                    slider.prev();
                });
            });
        }
        if (bundleOptions.controls.next) {
            bundleOptions.controls.next.addEventListener('click', function() {
                clearInterval(autoplayTimer);
                Object.keys(sliders).forEach(function(sliderId) {
                    var slider = sliders[sliderId];
                    slider.next();
                });
            });
        }
    }

    // handle autoplay
    if (bundleOptions.autoplay && bundleOptions.autoplay.enabled) {
        autoplayTimer = setInterval(function() {
            if (bundleOptions.autoplay.reverse) {
                return Object.keys(sliders).forEach(function(sliderId) {
                    var slider = sliders[sliderId];
                    slider.prev();
                });
            }
            Object.keys(sliders).forEach(function(sliderId) {
                var slider = sliders[sliderId];
                slider.next();
            });
        }, bundleOptions.autoplay.intervalMs);
    }

    ;[].forEach.call(slidersE, function(sliderE, index) {
        var slider = new Slider({
            sliderE: sliderE
        });
        sliders[index] = slider;
    });

    function Slider(sliderOptions) {
        var slides = {};
        var sliderE = sliderOptions.sliderE;
        var rect = sliderE.getBoundingClientRect();
        var scrollerE = findScroller();

        if (!scrollerE) return handleError(2);

        var slidesE = scrollerE.querySelectorAll(bundleOptions.slideSelector);
        ;[].forEach.call(slidesE, function(slideE, index) {
            var slide = new Slide({
                slideE: slideE,
                width: rect.width,
                dotE: dotsE[index]
            });
            slides[index] = slide;
        });

        this.currentSlideIndex = 0;

        this.next = function() {
            insert.call(this, 'next');
        };
        this.prev = function() {
            insert.call(this, 'prev');
        };
        this.slideTo = function(index) {
            if (index == this.currentSlideIndex) return;
            if (index < this.currentSlideIndex) {
                insert.call(this, 'prev', index);
            } else {
                insert.call(this, 'next', index);
            }
        };

        window.addEventListener('resize', function() {
            Object.keys(slides).forEach(function(slideId) {
                var slide = slides[slideId];
                slide.resize(sliderE.getBoundingClientRect().width);
            });
        });

        function insert(direction, index) {
            if (this.animating) return;

            if (bundleOptions.events && bundleOptions.events.onBeforeSlide) {
                bundleOptions.events.onBeforeSlide();
            }

            if (!index && direction == 'next') index = parseInt(this.currentSlideIndex) + 1;
            if (!index && direction == 'prev') index = parseInt(this.currentSlideIndex) - 1;

            // how many items in between
            var between = Math.abs(this.currentSlideIndex - index) + 1;
            scrollerE.style.width = between * 100 + '%';

            if (slides[this.currentSlideIndex].added) {
                this.slide(direction, between);

                // remove elements after sliding
                setTimeout(function() {
                    Object.keys(slides).forEach(function(slideIndex) {
                        if (slideIndex != this.currentSlideIndex && slides[slideIndex].added) {
                            slides[slideIndex].remove();
                        }
                        scrollerE.style.transition = '';
                        scrollerE.style.transform = 'translateX(0)';

                        if (bundleOptions.events && bundleOptions.events.onSlided) {
                            bundleOptions.events.onSlided();
                            if ( window.slObjectFit ) window.slObjectFit.initialize();
                        }
                    }.bind(this));
                }.bind(this), bundleOptions.animation.durationMs);
            }

            if (direction == 'next') {
                this.currentSlideIndex++;
                if (!slides[this.currentSlideIndex]) {
                    this.currentSlideIndex = 0;
                    slides[this.currentSlideIndex].append(scrollerE);
                } else {
                    for (var i = this.currentSlideIndex; i <= index; i++) {
                        slides[i].append(scrollerE);
                    }
                    if (index) this.currentSlideIndex = index;
                }
            }
            if (direction == 'prev') {
                this.currentSlideIndex--;
                if (!slides[this.currentSlideIndex]) {
                    this.currentSlideIndex = Object.keys(slides).length - 1;
                    slides[this.currentSlideIndex].prepend(scrollerE);
                } else {
                    for (var i = this.currentSlideIndex; i >= index; i--) {
                        slides[i].prepend(scrollerE);
                    }
                    if (index) this.currentSlideIndex = index;
                }
            }

            Object.keys(slides).forEach(function(slideId) {
                var slide = slides[slideId];
                slide.deactivate();
            });
            slides[this.currentSlideIndex].activate();
        }

        this.slide = function(direction, between) {
            this.animating = true;

            var amount = (between - 1) * (100 / between) + '%';

            if (direction == 'prev') scrollerE.style.transform = 'translateX(-' + amount + ')';

            setTimeout(function() {
                scrollerE.style.transition = 'transform ' + bundleOptions.animation.durationMs + 'ms ' + bundleOptions.animation.cssEasing + ' 0s';
                scrollerE.style.transform = (direction == 'next') ? 'translateX(-' + amount + ')' : 'translateX(0)';

                setTimeout(function() {
                    this.animating = false;
                }.bind(this), bundleOptions.animation.durationMs + 50);
            }.bind(this), 0);
        };

        slides[this.currentSlideIndex].append(scrollerE);
        slides[this.currentSlideIndex].activate();

        function findScroller() {
            var e;
            ;[].forEach.call(sliderOptions.sliderE.children, function(child) {
                if (e) return;
                if (child.matches(bundleOptions.scrollerSelector)) e = child;
            });
            return e;
        }
    };

    function Slide(slideOptions) {
        var rect = slideOptions.slideE.getBoundingClientRect();
        var slideE = slideOptions.slideE;

        slideE.style.width = slideOptions.width + 'px';

        this.getElement = function() {
            return slideE;
        };

        this.append = function(parentE) {
            this.added = true;
            parentE.appendChild(slideE);
        };

        this.prepend = function(parentE) {
            this.added = true;
            parentE.insertBefore(slideE, parentE.children[0]);
        };

        this.activate = function() {
            if (slideOptions.dotE) slideOptions.dotE.classList.add(bundleOptions.dotActiveClass);
        };

        this.deactivate = function() {
            slideOptions.dotE.classList.remove(bundleOptions.dotActiveClass);
        };

        this.resize = function(width) {
            slideE.style.width = width + 'px';
        };

        this.remove = function() {
            this.added = false;
            slideE.parentNode.removeChild(slideE);
        };

        this.remove();
    };

    function handleError(errorNumber) {
        switch (errorNumber) {
            case 1: console.error('Please define Slider-Options');
            case 2: console.error('The scrollerSelector is not correct, does not appear in DOM or your scroller is not an immediate child of your slider.');
        }
    }
};

/* element.matches polyfill */
if (!Element.prototype.matches) {
  Element.prototype.matches =
      Element.prototype.matchesSelector ||
      Element.prototype.mozMatchesSelector ||
      Element.prototype.msMatchesSelector ||
      Element.prototype.oMatchesSelector ||
      Element.prototype.webkitMatchesSelector ||
      function(s) {
        var matches = (this.document || this.ownerDocument).querySelectorAll(s),
            i = matches.length;
        while (--i >= 0 && matches.item(i) !== this) {}
        return i > -1;
      };
}
