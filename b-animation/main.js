PC3.define('primero/bricks/b-animation', ['pc3lib/jquery-3.4.1'], function($) {
	return {
		run: function() {
			
			var animationElements = document.querySelectorAll('.anim-element');
			var scrollTop;
			var scrollBottom;
			
			// console.log(animationElements);
			
			// precalculate heights and tops
			;[].forEach.call(animationElements, function(animationElement) {
				animationElement.top = $(animationElement).offset().top;
				animationElement.height = $(animationElement).height();
			});
			
			window.addEventListener('scroll', triggerAnimation);
			window.addEventListener('resize', triggerAnimation);
			triggerAnimation();
			
			// not throttled to ensure functionality
			function triggerAnimation() {
				// logic
				scrollTop = $(document).scrollTop();
				scrollBottom = scrollTop + Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
				
				;[].forEach.call(animationElements, function(animationElement) {
					var shouldAnimate = scrollTop <= animationElement.top + animationElement.height && scrollBottom >= animationElement.top;
					if (shouldAnimate) {
						window.requestAnimationFrame(function() {
							animationElement.classList.remove('anim-start');
						});
					}
				});
			}
		}
	}
});